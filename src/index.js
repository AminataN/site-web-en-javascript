'use strict'
const express = require('express')
const app = express()
const fs = require('fs')
let nunjucks = require('nunjucks')
const nouv_module = require('jsau-module')
const morgan = require('morgan')
app.use(morgan('dev'))

const body = require('body-parser')

nunjucks.configure('./src/assets', {
    express: app,
    autoescape: true
})

const my_shared_code_headless = require('./my_shared_code_headless')

let even_numbers = my_shared_code_headless.generateEvenNumbers(20)
console.log('even_numbers:', even_numbers)


app.use(body.json())
app.use(body.urlencoded({extended: true}))

app.use(express.static('./src/assets/images'))


//la liste des nouvelles
app.get('/', (req, res) => {
    fs.readFile('./src/nouvelles.json', {encoding: 'utf8'}, (err, data) => {
        if (err) {
            res.send(500)
        } else {
            try {
                let donnee = JSON.parse(data)
                //eslint-disable-next-line no-console
                res.status(200).render('index.njk', {lesNouvelles: donnee, categories: nouv_module.categorie})
            } catch (err) {
                //eslint-disable-next-line no-console
                console.error('Invalid JSON in file:', err)
                res.sendStatus(500)
            }
        }
    })
})

// Ajout d'une nouvelle
app.post('/', (req, res) => {
    let postdata = req.body
    console.log(postdata)
    fs.readFile('./src/nouvelles.json', {encoding: 'utf8'}, (err, data) => {
        if (err) {
            console.error('Unable to read file')
        } else {
            try {
                let rawdata = JSON.parse(data)
                let newid = rawdata.length + 1
                let newobject = {
                    id: newid,
                    nom: postdata.nom,
                    categorie: postdata.categorie,
                    description: postdata.description,
                    auteur: postdata.auteur,
                    date: postdata.date
                }
                try {
                        rawdata.push(newobject) // insert post data
                        let formatdata = JSON.stringify(rawdata, null, 2) // convert back to json
                        fs.writeFile('./src/nouvelles.json', formatdata, (err) => {
                            if (err) {
                                console.error('Unable to write in file', err)
                                res.sendStatus(500)
                            } else {
                                res.status(201).redirect('/success/1')
                            }
                        })
                } catch (err) {
                    res.status(404).send(err)
                }
            } catch (err) {
                console.error('Invalid JSON in file:', err)
                res.sendStatus(500)
            }
        }
    })

})
// On gère ici les trois type de succès
// 1 pour l'ajout d'une nouvelle
// 2 pour la modification d'une nouvelle
app.get('/success/:nb', (req, res) => {
    if (req.params.nb == 1) {
        res.status(200).render('success.njk', {message: 'La nouvelle a bien été ajoutée'})
    } else if (req.params.nb == 2) {
        res.status(200).render('success.njk', {message: 'La nouvelle a bien été modifiée'})
    } else {
        res.status(200).render('success.njk', {message: 'La nouvelle a bien été suprimée'})
    }
})
// Rendu pré-mise à jour d'une ressource
// on renvoi la ressource dans une form
// avec la possibilité de modification des champs
app.get('/edit/:id', (req, res) => {

    fs.readFile('./src/nouvelles.json', {encoding: 'utf8'}, (err, data) => {
        if (err) {
            res.sendStatus(500)
        } else {
            try {
                let rawdata = JSON.parse(data)
                let idnouvelle = req.params.id
                let nouvelle = rawdata.find((item) => {
                    return item.id == idnouvelle
                })
                if (nouvelle) {
                    res.status(200).render('edit.njk', {lesNouvelles: rawdata, categories: nouv_module.categorie})
                } else {
                    res.status(404).send('NOUVELLE NOT FOUND')
                }
            } catch (err) {
                res.sendStatus(500)
            }
        }
    })
})
// Suppression d'une ressource
app.get('/delete/:id', (req, res) => {

    fs.readFile('./src/nouvelles.json', {encoding: 'utf8'}, (err, data) => {
        if (err) {
            res.sendStatus(500)
        } else {
            try {
                let rawdata = JSON.parse(data)
                const idnouvelle = req.params.id
                const nouvelleindex = rawdata.findIndex((item) => {
                    return item.id == idnouvelle
                })
                if (nouvelleindex > -1) {
                    rawdata.splice(nouvelleindex, 1)
                    let formatdata = JSON.stringify(rawdata, null, 2) // convert back to json
                    fs.writeFile('./src/nouvelles.json', formatdata, (err) => {
                        if (err) {
                            console.error('Unable to write file')
                            res.sendStatus(500)
                        } else {
                            res.status(201).redirect('/success/4')
                        }
                    })
                } else {
                    res.status(404).send('NO SUCH NOUVELLE WITH THAT ID')
                }
            } catch (err) {
                res.sendStatus(500)
            }
        }
    })
})
// Mise à jour proprement dit  d'une ressource
app.post('/updatenew', (req, res) => {
    let updatedata = req.body
    console.log(updatedata)
    fs.readFile('./src/nouvelles.json', {encoding: 'utf8'}, (err, data) => {
        if (err) {
            res.sendStatus(500)
        } else {
            try {
                let rawdata = JSON.parse(data)
                const idnouvelle = updatedata.id
                const nouvelleindex = rawdata.findIndex((item) => {
                    return item.id == idnouvelle
                })
                if (nouvelleindex > -1) {
                    rawdata.splice(nouvelleindex, 1)
                    let newobject = {
                        id: updatedata.id,
                        nom: updatedata.nom,
                        categorie: updatedata.categorie,
                        description: updatedata.description,
                        auteur: updatedata.auteur,
                        date: updatedata.date
                    }
                    rawdata.push(newobject)
                    let formatdata = JSON.stringify(rawdata, null, 2) // convert back to json
                    fs.writeFile('./src/nouvelles.json', formatdata, (err) => {
                        if (err) {
                            console.error('Unable to write file', err)
                            res.sendStatus(500)
                        } else {
                            res.status(201).redirect('/success/2')
                        }
                    })
                } else {
                    res.status(404).send('NOUVELLE NOT FOUND')
                }
            } catch (err) {
                console.error('OOPS SOMETHING HAPPENED', err)
                res.sendStatus(500)
            }
        }
    })
})

// Info
app.get('/info', (req, res) => {
    res.send('jsau-webserver-1.0.0')
})


app.listen(8081)
